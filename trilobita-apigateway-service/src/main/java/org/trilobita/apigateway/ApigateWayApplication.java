package org.trilobita.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * Created by quxiangqian on 2018/3/13.
 */
@EnableZuulProxy
@SpringBootApplication
public class ApigateWayApplication {
    public static void main( String[] args ) {
        SpringApplication.run(ApigateWayApplication.class, args);
    }
}
